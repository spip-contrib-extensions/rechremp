<?php
/**
 * Définit les autorisations du plugin Info SPIP
 *
 * @plugin     Rechercher Remplacer
 * @copyright  2013-2016
 * @author     Teddy Payet
 * @licence    GNU/GPL
 * @package    SPIP\rechremp\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser
 */
function rechremp_autoriser() {
}


/**
 * Autorisation de voir `rechremp`
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_rechremp_voir_dist($faire, $type, $id, $qui, $opt) {
    return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}

/**
 * Autorisation de configurer `rechremp`
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_rechremp_configurer_dist($faire, $type, $id, $qui, $opt) {
    return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}

